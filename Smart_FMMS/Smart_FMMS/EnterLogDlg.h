#pragma once
#include "afxcmn.h"
#include "EnterLog.h"
// CEnterLogDlg 대화 상자입니다.

class CEnterLogDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CEnterLogDlg)

public:
	CEnterLogDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CEnterLogDlg();
	CEnterLog* m_entSet;
	inline void InitDatabase(CEnterLog* entSet) {
		m_entSet = entSet;
	}

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ENTERLOG_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_DBView;
	void UpdateDBView();
	virtual BOOL OnInitDialog();
protected:
	bool _inited;
};
