//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// Smart_FMMS.rc에서 사용되고 있습니다.
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_SMART_FMMS_FORM             101
#define IDP_FAILED_OPEN_DATABASE        103
#define IDP_SOCKETS_INIT_FAILED         104
#define IDR_MAINFRAME                   128
#define IDR_Smart_FMMSTYPE              130
#define IDD_EQUIPMENT_DIALOG            313
#define IDD_CLOSUREEQUIPMENTINFO_DIALOG 313
#define IDD_EVENTLOG_DIALOG             316
#define IDD_INSTALLEQUIPMENTINFO_DIALOG 319
#define IDD_INSTALLEDEQUIPMENTINFO_DIALOG 319
#define IDD_ENTERLOG_DIALOG             323
#define IDD_CLOSURESTATE_DIALOG         324
#define IDC_DBVIEW                      1000
#define IDC_GROUPNAME1                  1005
#define IDC_GROUPNAME2                  1006
#define IDC_QRCODE                      1007
#define IDC_LOCATIONNAME                1008
#define IDC_CLOSUREID                   1009
#define IDC_INSTALLATIONCLASSIFICATION  1010
#define IDC_INSTALLATIONADDRESS         1011
#define IDC_LATITUDE                    1012
#define IDC_LONGITUDE                   1013
#define IDC_IPADDRESS                   1014
#define IDC_INSTALLATIONCOMPANY         1016
#define IDC_MAINTENANCECOMPANY          1017
#define IDC_COMPANYTELEPHONE1           1019
#define IDC_COMPANYTELEPHONE2           1020
#define IDC_COMPANYTELEPHONE3           1021
#define IDC_ADDGROUP                    1032
#define IDC_PORT                        1034
#define IDC_COMPLETIONDATE              1035
#define IDC_AVAILABILITY                1036
#define IDC_INSTALLATION_BUTTON         1042
#define IDC_INSTALLED_BUTTON            1042
#define IDC_EVENTLOG_BUTTON             1043
#define IDC_ENTERLOG_BUTTON             1044
#define IDC_EQUIPMENT_BUTTON            1045
#define IDC_STATE_BUTTON                1046
#define IDC_DELETE                      1048
#define IDC_ADD                         1049
#define IDC_EDIT                        1050
#define IDC_EQUIPMENTTYPE               1051
#define IDC_MANUFACTURER                1052
#define IDC_PRICE                       1053
#define IDC_MODELNAME                   1054
#define IDC_SPECIFICATION               1055
#define IDC_LIST1                       1056
#define IDC_STATE                       1057
#define IDC_LOAD                        1060
#define IDC_BUTTON1                     1062
#define IDC_DEPARTMENT                  1066
#define IDC_PASSWORD                    1071
#define ID_BUTTON2                      32771

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        326
#define _APS_NEXT_COMMAND_VALUE         32772
#define _APS_NEXT_CONTROL_VALUE         1079
#define _APS_NEXT_SYMED_VALUE           310
#endif
#endif
