
// Smart_FMMSSet.cpp : CSmart_FMMSSet 클래스의 구현
//

#include "stdafx.h"
#include "Smart_FMMS.h"
#include "ClosureInfo.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CSmart_FMMSSet 구현

// 코드 생성 위치 2018년 11월 24일 토요일, 오전 1:14

IMPLEMENT_DYNAMIC(CClosureInfo, CRecordset)

CClosureInfo::CClosureInfo(CDatabase* pdb)
	: CRecordset(pdb)
{
	m_Index = 0;
	m_GroupName1 = L"";
	m_GroupName2 = L"";
	m_QRCode = L"";
	m_ClosureID = 0;
	m_State = L"";
	m_LocationName = L"";
	m_InstallationClassification = L"";
	m_InstallationAddress = L"";
	m_latitude = L"";
	m_longitude = L"";
	m_IPAddress = L"";
	m_Port = 0;
	m_CompletionDate;
	m_InstallationCompany = L"";
	m_MaintenanceCompany = L"";
	m_CompanyTelephone = L"";
	m_Availability = FALSE;
	m_nFields = 18;
	m_nDefaultType = dynaset;
}
// 아래 연결 문자열에 일반 텍스트 암호 및/또는 
// 다른 중요한 정보가 포함되어 있을 수 있습니다.
// 보안 관련 문제가 있는지 연결 문자열을 검토한 후에 #error을(를) 제거하십시오.
// 다른 형식으로 암호를 저장하거나 다른 사용자 인증을 사용하십시오.
CString CClosureInfo::GetDefaultConnect()
{
	return _T("DSN=Smart_FMMS;DBQ=DatabaseDesignEx.mdb;DriverId=25;FIL=MS Access;MaxBufferSize=2048;PageTimeout=5;UID=admin;");
}

CString CClosureInfo::GetDefaultSQL()
{
	return _T("[함체정보]");
}

void CClosureInfo::DoFieldExchange(CFieldExchange* pFX)
{
	pFX->SetFieldType(CFieldExchange::outputColumn);
// RFX_Text() 및 RFX_Int() 같은 매크로는 데이터베이스의 필드
// 형식이 아니라 멤버 변수의 형식에 따라 달라집니다.
// ODBC에서는 자동으로 열 값을 요청된 형식으로 변환하려고 합니다
	RFX_Long(pFX, _T("[Index]"), m_Index);
	RFX_Text(pFX, _T("[GroupName1]"), m_GroupName1);
	RFX_Text(pFX, _T("[GroupName2]"), m_GroupName2);
	RFX_Text(pFX, _T("[QRCode]"), m_QRCode);
	RFX_Long(pFX, _T("[ClosureID]"), m_ClosureID);
	RFX_Text(pFX, _T("[State]"), m_State);
	RFX_Text(pFX, _T("[LocationName]"), m_LocationName);
	RFX_Text(pFX, _T("[InstallationClassification]"), m_InstallationClassification);
	RFX_Text(pFX, _T("[InstallationAddress]"), m_InstallationAddress);
	RFX_Text(pFX, _T("[latitude]"), m_latitude);
	RFX_Text(pFX, _T("[longitude]"), m_longitude);
	RFX_Text(pFX, _T("[IPAddress]"), m_IPAddress);
	RFX_Long(pFX, _T("[Port]"), m_Port);
	RFX_Date(pFX, _T("[CompletionDate]"), m_CompletionDate);
	RFX_Text(pFX, _T("[InstallationCompany]"), m_InstallationCompany);
	RFX_Text(pFX, _T("[MaintenanceCompany]"), m_MaintenanceCompany);
	RFX_Text(pFX, _T("[CompanyTelephone]"), m_CompanyTelephone);
	RFX_Bool(pFX, _T("[Availability]"), m_Availability);

}
/////////////////////////////////////////////////////////////////////////////
// CSmart_FMMSSet 진단

#ifdef _DEBUG
void CClosureInfo::AssertValid() const
{
	CRecordset::AssertValid();
}

void CClosureInfo::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG

