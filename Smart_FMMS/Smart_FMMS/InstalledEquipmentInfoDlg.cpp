// InstalledEquipmentInfoDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Smart_FMMS.h"
#include "InstalledEquipmentInfoDlg.h"
#include "afxdialogex.h"


// CInstalledEquipmentInfoDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CInstalledEquipmentInfoDlg, CDialogEx)

CInstalledEquipmentInfoDlg::CInstalledEquipmentInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_INSTALLEDEQUIPMENTINFO_DIALOG, pParent)
	, _inited(false)
{

}

CInstalledEquipmentInfoDlg::~CInstalledEquipmentInfoDlg()
{

}

void CInstalledEquipmentInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DBVIEW, m_DBView);
	DDX_Control(pDX, IDC_AVAILABILITY, m_Availability);
}


BEGIN_MESSAGE_MAP(CInstalledEquipmentInfoDlg, CDialogEx)
	ON_BN_CLICKED(IDC_LOAD, &CInstalledEquipmentInfoDlg::OnBnClickedLoad)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_DBVIEW, &CInstalledEquipmentInfoDlg::OnLvnItemchangedDbview)
	ON_BN_CLICKED(IDC_ADD, &CInstalledEquipmentInfoDlg::OnBnClickedAdd)
	ON_BN_CLICKED(IDC_EDIT, &CInstalledEquipmentInfoDlg::OnBnClickedEdit)
	ON_BN_CLICKED(IDC_DELETE, &CInstalledEquipmentInfoDlg::OnBnClickedDelete)
END_MESSAGE_MAP()


// CInstalledEquipmentInfoDlg 메시지 처리기입니다.


void CInstalledEquipmentInfoDlg::UpdateDBView()
{
	static short int width = 100;
	static LPCTSTR name[] = 
	{ _T("일련번호"),_T("함체위치명"),_T("장비Type") ,_T("상태") ,_T("담당부서") ,_T("제조사명"),
		_T("모델명"),_T("IP주소"),_T("포트번호"),_T("비밀번호"),_T("설치업체"),_T("유지보수업체"),
		_T("업체연락처"),_T("사용여부") };
	if (!_inited) {
		for (int i = 0; i < 14; i++) {
			m_DBView.InsertColumn(i + 1, name[i], LVCFMT_LEFT, width);
		}
		_inited = 1;
	}
	m_DBView.DeleteAllItems();
	CInstalledEquipmentInfo ie_info(m_ieSet->m_pDatabase);
	ie_info.Open(CRecordset::dynaset, ie_info.GetDefaultSQL());
	for (int i = 0; !ie_info.IsEOF(); ie_info.MoveNext(),i++) {
		CString itemStr;
		itemStr.Format(_T("%d"), ie_info.m_Index);
		m_DBView.InsertItem(i, itemStr);
		m_DBView.SetItemText(i, 1, ie_info.m_LocationName);
		m_DBView.SetItemText(i, 2, ie_info.m_EquipmentType);
		m_DBView.SetItemText(i, 3, ie_info.m_State);
		m_DBView.SetItemText(i, 4, ie_info.m_Department);
		m_DBView.SetItemText(i, 5, ie_info.m_Manufacturer);
		m_DBView.SetItemText(i, 6, ie_info.m_ModelName);
		m_DBView.SetItemText(i, 7, ie_info.m_IPAddress);
		if (ie_info.m_Port >= 0 && ie_info.m_Port <= 65535) {
			itemStr.Format(_T("%d"), ie_info.m_Port);
			m_DBView.SetItemText(i, 8, itemStr);
		}
		m_DBView.SetItemText(i, 9, ie_info.m_Password);
		m_DBView.SetItemText(i, 10, ie_info.m_InstallationCompany);
		m_DBView.SetItemText(i, 11, ie_info.m_MaintenanceCompany);
		m_DBView.SetItemText(i, 12, ie_info.m_CompanyTelephone);
		m_DBView.SetItemText(i, 13, ie_info.m_Availability?_T("YES"):_T("NO"));
	}
	ie_info.Close();
}


void CInstalledEquipmentInfoDlg::OnBnClickedLoad()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateDBView();
}


BOOL CInstalledEquipmentInfoDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	UpdateDBView();
	m_DBView.SetExtendedStyle(LVS_EX_FULLROWSELECT);
	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CInstalledEquipmentInfoDlg::OnLvnItemchangedDbview(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
    int	index = pNMLV->iItem;
	if ((m_DBView.GetItemState(index, LVIS_SELECTED) & LVIS_SELECTED) && index >= 0 && index <= m_DBView.GetItemCount()){
		m_index = _ttoi(m_DBView.GetItemText(index, 0));
		SetDlgItemText(IDC_LOCATIONNAME, m_DBView.GetItemText(index, 1));
		SetDlgItemText(IDC_EQUIPMENTTYPE, m_DBView.GetItemText(index, 2));
		SetDlgItemText(IDC_STATE, m_DBView.GetItemText(index, 3));
		SetDlgItemText(IDC_DEPARTMENT, m_DBView.GetItemText(index, 4));
		SetDlgItemText(IDC_MANUFACTURER, m_DBView.GetItemText(index, 5));
		SetDlgItemText(IDC_MODELNAME, m_DBView.GetItemText(index, 6));
		SetDlgItemText(IDC_IPADDRESS, m_DBView.GetItemText(index, 7));
		SetDlgItemText(IDC_PORT, m_DBView.GetItemText(index, 8));
		SetDlgItemText(IDC_PASSWORD, m_DBView.GetItemText(index, 9));
		SetDlgItemText(IDC_INSTALLATIONCOMPANY, m_DBView.GetItemText(index, 10));
		SetDlgItemText(IDC_MAINTENANCECOMPANY, m_DBView.GetItemText(index, 11));
		CString phone_number = m_DBView.GetItemText(index, 12), token;
		int pos = 0;
		if (phone_number.GetLength() >= 10 &&
			phone_number.Find(_T("-")) >= 0) {
			if ((token = phone_number.Tokenize(_T("-"), pos)) != "") {
				SetDlgItemText(IDC_COMPANYTELEPHONE1, token);
			}
			if ((token = phone_number.Tokenize(_T("-"), pos)) != "") {
				SetDlgItemText(IDC_COMPANYTELEPHONE2, token);
			}
			if ((token = phone_number.Tokenize(_T("-"), pos)) != "") {
				SetDlgItemText(IDC_COMPANYTELEPHONE3, token);
			}
		}
	}
	*pResult = 0;
}


void CInstalledEquipmentInfoDlg::OnBnClickedAdd()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CInstalledEquipmentInfo ie_info(m_ieSet->m_pDatabase);
	if (!ie_info.Open()) {
		return;
	}
	ie_info.AddNew();
	ie_info.m_Index = _ttoi(m_DBView.GetItemText(m_DBView.GetItemCount() - 1 , 0)) + 1;
	GetDlgItemText(IDC_LOCATIONNAME, ie_info.m_LocationName);
	GetDlgItemText(IDC_EQUIPMENTTYPE, ie_info.m_EquipmentType);
	GetDlgItemText(IDC_STATE, ie_info.m_State);
	GetDlgItemText(IDC_DEPARTMENT, ie_info.m_Department);
	GetDlgItemText(IDC_MANUFACTURER, ie_info.m_Manufacturer);
	GetDlgItemText(IDC_IPADDRESS, ie_info.m_IPAddress);
	CString port_str;
	GetDlgItemText(IDC_PORT, port_str);
	ie_info.m_Port = _ttoi(port_str);
	GetDlgItemText(IDC_PASSWORD, ie_info.m_Password);
	GetDlgItemText(IDC_INSTALLATIONCOMPANY, ie_info.m_InstallationCompany);
	GetDlgItemText(IDC_MAINTENANCECOMPANY, ie_info.m_MaintenanceCompany);
	CString phone[3];
	GetDlgItemText(IDC_COMPANYTELEPHONE1, phone[0]);
	GetDlgItemText(IDC_COMPANYTELEPHONE2, phone[1]);
	GetDlgItemText(IDC_COMPANYTELEPHONE3, phone[2]);
    ie_info.m_CompanyTelephone = phone[0] + "-" + phone[1] + "-" + phone[2];
	ie_info.m_Availability = m_Availability.GetCheck();
	if (!ie_info.Update()) {
		AfxMessageBox(_T("Record not added; no field values were set."));
	}
	ie_info.Close();
	m_ieSet->Open();
	m_ieSet->Requery();
	m_ieSet->Close();
	UpdateDBView();
}


void CInstalledEquipmentInfoDlg::OnBnClickedEdit()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CInstalledEquipmentInfo ie_info(m_ieSet->m_pDatabase);
	CString query;
	query.Format(_T("%s WHERE Index = %d"), ie_info.GetDefaultSQL(), m_index);
	if (!ie_info.Open(CRecordset::dynaset,query)) {
		return;
	}
	ie_info.Edit();
	GetDlgItemText(IDC_LOCATIONNAME, ie_info.m_LocationName);
	GetDlgItemText(IDC_EQUIPMENTTYPE, ie_info.m_EquipmentType);
	GetDlgItemText(IDC_STATE, ie_info.m_State);
	GetDlgItemText(IDC_DEPARTMENT, ie_info.m_Department);
	GetDlgItemText(IDC_MANUFACTURER, ie_info.m_Manufacturer);
	GetDlgItemText(IDC_IPADDRESS, ie_info.m_IPAddress);
	CString port_str;
	GetDlgItemText(IDC_PORT, port_str);
	ie_info.m_Port = _ttoi(port_str);
	GetDlgItemText(IDC_PASSWORD, ie_info.m_Password);
	GetDlgItemText(IDC_INSTALLATIONCOMPANY, ie_info.m_InstallationCompany);
	GetDlgItemText(IDC_MAINTENANCECOMPANY, ie_info.m_MaintenanceCompany);
	CString phone[3];
	GetDlgItemText(IDC_COMPANYTELEPHONE1, phone[0]);
	GetDlgItemText(IDC_COMPANYTELEPHONE2, phone[1]);
	GetDlgItemText(IDC_COMPANYTELEPHONE3, phone[2]);
	ie_info.m_CompanyTelephone = phone[0] + "-" + phone[1] + "-" + phone[2];
	ie_info.m_Availability = m_Availability.GetCheck();
	if (!ie_info.Update())
	{
		AfxMessageBox(_T("Record not updated; no field values were set."));
	}
	ie_info.Close();
	m_ieSet->Open();
	m_ieSet->Requery();
	m_ieSet->Close();
	UpdateDBView();
}


void CInstalledEquipmentInfoDlg::OnBnClickedDelete()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CInstalledEquipmentInfo ie_info(m_ieSet->m_pDatabase);
	CString query;
	query.Format(_T("%s WHERE Index = %d"), ie_info.GetDefaultSQL(), m_index);
	if (!ie_info.Open(CRecordset::dynaset, query)) {
		return;
	}
	if (ie_info.CanUpdate()) {
		ie_info.Delete();
		ie_info.MoveNext();
	}
	ie_info.Close();
	m_ieSet->Open();
	m_ieSet->Requery();
	m_ieSet->Close();
	UpdateDBView();
}
