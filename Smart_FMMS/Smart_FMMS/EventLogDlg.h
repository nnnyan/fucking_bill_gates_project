#pragma once
#include "afxcmn.h"
#include "EventLog.h"
// CEventLogDlg 대화 상자입니다.

class CEventLogDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CEventLogDlg)

public:
	CEventLogDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CEventLogDlg();
	CEventLog* m_evtSet;
	inline void InitDatabase(CEventLog* evtSet) {
		m_evtSet = evtSet;
	}
// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_EVENTLOG_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_DBView;
	void UpdateDBView();
	virtual BOOL OnInitDialog();
protected:
	bool _inited;
};
