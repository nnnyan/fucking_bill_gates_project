// EventLogDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Smart_FMMS.h"
#include "EventLogDlg.h"
#include "afxdialogex.h"


// CEventLogDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CEventLogDlg, CDialogEx)

CEventLogDlg::CEventLogDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_EVENTLOG_DIALOG, pParent)
	, _inited(false)
{

}

CEventLogDlg::~CEventLogDlg()
{

}

void CEventLogDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DBVIEW, m_DBView);
}


BEGIN_MESSAGE_MAP(CEventLogDlg, CDialogEx)
END_MESSAGE_MAP()


// CEventLogDlg 메시지 처리기입니다.


void CEventLogDlg::UpdateDBView()
{
	static short int width = 100;
	static LPCTSTR name[] = 
	{ _T("그룹이름1"),_T("그룹이름2"),_T("함체위치명"),_T("이벤트"),_T("이벤트 내용") ,_T("발생일시")};
	if (!_inited) {
		for (int i = 0; i < 6; i++) {
			m_DBView.InsertColumn(i + 1, name[i], LVCFMT_LEFT, width);
		}
		_inited = 1;
	}
	m_DBView.DeleteAllItems();
	CEventLog evlog(m_evtSet->m_pDatabase);
	evlog.Open(CRecordset::dynaset, evlog.GetDefaultSQL());
	for (int i = 0; !evlog.IsEOF(); evlog.MoveNext(),i++) {
		m_DBView.InsertItem(i,evlog.m_GroupName1);
		m_DBView.SetItemText(i, 1, evlog.m_GroupName2);
		m_DBView.SetItemText(i, 2, evlog.m_LocationName);
		m_DBView.SetItemText(i, 3, evlog.m_Event);
		m_DBView.SetItemText(i, 4, evlog.m_EventDescription);
		CString itemStr;
		itemStr.Format(_T("%d-%d-%d"),
			evlog.m_OccurrenceDate.GetYear(),
			evlog.m_OccurrenceDate.GetMonth(),
			evlog.m_OccurrenceDate.GetDay());
		m_DBView.SetItemText(i, 5, itemStr);
	}
	evlog.Close();
}


BOOL CEventLogDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	UpdateDBView();
	m_DBView.SetExtendedStyle(LVS_EX_FULLROWSELECT);
	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
