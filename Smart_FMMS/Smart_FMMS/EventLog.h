// EventLog.h : CEventLog의 선언입니다.

#pragma once

// 코드 생성 위치 2018년 11월 25일 일요일, 오전 12:26

class CEventLog : public CRecordset
{
public:
	CEventLog(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CEventLog)

// 필드/매개 변수 데이터

// 아래의 문자열 형식(있을 경우)은 데이터베이스 필드의 실제 데이터 형식을
// 나타냅니다(CStringA:ANSI 데이터 형식, CStringW: 유니코드 데이터 형식).
// 이것은 ODBC 드라이버에서 불필요한 변환을 수행할 수 없도록 합니다.  // 원할 경우 이들 멤버를 CString 형식으로 변환할 수 있으며
// 그럴 경우 ODBC 드라이버에서 모든 필요한 변환을 수행합니다.
// (참고: 유니코드와 이들 변환을 모두 지원하려면  ODBC 드라이버
// 버전 3.5 이상을 사용해야 합니다).

	CStringW	m_GroupName1;	//그룹 이름 1
	CStringW	m_GroupName2;	//그룹 이름 2
	CStringW	m_LocationName;	//함체위치명
	CStringW	m_Event;	//이벤트
	CStringW	m_EventDescription;	//이벤트 내용
	CTime	m_OccurrenceDate;	//발생일시

// 재정의
	// 마법사에서 생성한 가상 함수 재정의
	public:
	virtual CString GetDefaultConnect();	// 기본 연결 문자열

	virtual CString GetDefaultSQL(); 	// 레코드 집합의 기본 SQL
	virtual void DoFieldExchange(CFieldExchange* pFX);	// RFX 지원

// 구현
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

};


