// ClosureEquipmentInfoDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Smart_FMMS.h"
#include "ClosureEquipmentInfoDlg.h"
#include "afxdialogex.h"


// CClosureEquipmentInfoDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CClosureEquipmentInfoDlg, CDialogEx)

CClosureEquipmentInfoDlg::CClosureEquipmentInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_CLOSUREEQUIPMENTINFO_DIALOG, pParent)
	, _inited(false)
{

}

CClosureEquipmentInfoDlg::~CClosureEquipmentInfoDlg()
{
}

void CClosureEquipmentInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DBVIEW, m_DBView);
	DDX_Control(pDX, IDC_EQUIPMENTTYPE, m_EquipmentType);
	DDX_Control(pDX, IDC_MANUFACTURER, m_Manufacturer);
	DDX_Control(pDX, IDC_MODELNAME, m_ModelName);
	DDX_Control(pDX, IDC_SPECIFICATION, m_Specification);
}


BEGIN_MESSAGE_MAP(CClosureEquipmentInfoDlg, CDialogEx)
	ON_BN_CLICKED(IDC_LOAD, &CClosureEquipmentInfoDlg::OnBnClickedLoad)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_DBVIEW, &CClosureEquipmentInfoDlg::OnLvnItemchangedDbview)
	ON_BN_CLICKED(IDC_ADD, &CClosureEquipmentInfoDlg::OnBnClickedAdd)
	ON_BN_CLICKED(IDC_EDIT, &CClosureEquipmentInfoDlg::OnBnClickedEdit)
	ON_BN_CLICKED(IDC_DELETE, &CClosureEquipmentInfoDlg::OnBnClickedDelete)
END_MESSAGE_MAP()


// CClosureEquipmentInfoDlg 메시지 처리기입니다.


void CClosureEquipmentInfoDlg::UpdateDBView()
{
	static short int width = 100;
	static LPCTSTR name[] = { _T("일련번호"), _T("장비Type"),_T("제조사"),_T("모델명") ,_T("가격") ,_T("제원") };
	if (!_inited) {
		for (int i = 0; i < 6; i++) {
			m_DBView.InsertColumn(i + 1, name[i], LVCFMT_LEFT, width);
		}
		_inited = 1;
	}
	m_DBView.DeleteAllItems();
	CClosureEquipmentInfo ce_info(m_ceSet->m_pDatabase);
	ce_info.Open(CRecordset::dynaset, ce_info.GetDefaultSQL());
	for (int i = 0; !ce_info.IsEOF(); ce_info.MoveNext(), i++) {
		CString index_str;
		index_str.Format(_T("%d"), ce_info.m_Index);
		m_DBView.InsertItem(i, index_str);
		m_DBView.SetItemText(i, 1, ce_info.m_EquipmentType);
		m_DBView.SetItemText(i, 2, ce_info.m_Manufacturer);
		m_DBView.SetItemText(i, 3, ce_info.m_ModelName);
		if (ce_info.m_Price >= 0) {
			CString itemStr;
			itemStr.Format(_T("%d"), ce_info.m_Price);
			m_DBView.SetItemText(i, 4, itemStr);
		}
		m_DBView.SetItemText(i, 5, ce_info.m_Specification);
	}
	ce_info.Close();
}


void CClosureEquipmentInfoDlg::OnBnClickedLoad()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CClosureEquipmentInfo ce_info(m_ceSet->m_pDatabase);
	UpdateDBView();
}


BOOL CClosureEquipmentInfoDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	UpdateDBView();
	m_DBView.SetExtendedStyle(LVS_EX_FULLROWSELECT);
	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CClosureEquipmentInfoDlg::OnLvnItemchangedDbview(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int index = pNMLV->iItem;
	if ((m_DBView.GetItemState(index, LVIS_SELECTED) & LVIS_SELECTED) && index >= 0 && index <= m_DBView.GetItemCount()) {
		m_index = _ttoi(m_DBView.GetItemText(index, 0));
		SetDlgItemText(IDC_EQUIPMENTTYPE, m_DBView.GetItemText(index, 1));
		SetDlgItemText(IDC_MANUFACTURER, m_DBView.GetItemText(index, 2));
		SetDlgItemText(IDC_MODELNAME, m_DBView.GetItemText(index, 3));
		SetDlgItemText(IDC_PRICE, m_DBView.GetItemText(index, 4));
		SetDlgItemText(IDC_SPECIFICATION, m_DBView.GetItemText(index, 5));
	}
	*pResult = 0;
}


void CClosureEquipmentInfoDlg::OnBnClickedAdd()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CClosureEquipmentInfo ce_info(m_ceSet->m_pDatabase);
	if (!ce_info.Open()) {
		return;
	}
	ce_info.AddNew();
	ce_info.m_Index = _ttoi(m_DBView.GetItemText(m_DBView.GetItemCount() - 1,0)) + 1;
	GetDlgItemText(IDC_EQUIPMENTTYPE, ce_info.m_EquipmentType);
	GetDlgItemText(IDC_MANUFACTURER, ce_info.m_Manufacturer);
	GetDlgItemText(IDC_MODELNAME, ce_info.m_ModelName);
	CString price;
	GetDlgItemText(IDC_PRICE, price);
	ce_info.m_Price = _ttoi(price);
	GetDlgItemText(IDC_SPECIFICATION, ce_info.m_Specification);
	if (!ce_info.Update()) {
		AfxMessageBox(_T("Record not added; no field values were set."));
	}
	ce_info.Close();
	m_ceSet->Open();
	m_ceSet->Requery();
	m_ceSet->Close();
	UpdateDBView();
}


void CClosureEquipmentInfoDlg::OnBnClickedEdit()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CClosureEquipmentInfo ce_info(m_ceSet->m_pDatabase);
	CString query, model_name;
	GetDlgItemText(IDC_MODELNAME, model_name);
	query.Format(_T("%s WHERE Index = %d"), ce_info.GetDefaultSQL(), m_index);
	if (!ce_info.Open(CRecordset::dynaset, query)) {
		return;
	}
	ce_info.Edit();
	GetDlgItemText(IDC_EQUIPMENTTYPE, ce_info.m_EquipmentType);
	GetDlgItemText(IDC_MANUFACTURER, ce_info.m_Manufacturer);
	GetDlgItemText(IDC_MODELNAME, ce_info.m_ModelName);
	CString price;
	GetDlgItemText(IDC_PRICE, price);
	ce_info.m_Price = _ttoi(price);
	GetDlgItemText(IDC_SPECIFICATION, ce_info.m_Specification);
	if (!ce_info.Update())
	{
		AfxMessageBox(_T("Record not updated; no field values were set."));
	}	
	ce_info.Close();
	m_ceSet->Open();
	m_ceSet->Requery();
	m_ceSet->Close();
	UpdateDBView();
}


void CClosureEquipmentInfoDlg::OnBnClickedDelete()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CClosureEquipmentInfo ce_info(m_ceSet->m_pDatabase);
	CString query,model_name;
	GetDlgItemText(IDC_MODELNAME, model_name);
	query.Format(_T("%s WHERE Index = %d"), ce_info.GetDefaultSQL(), m_index);
	if (!ce_info.Open(CRecordset::dynaset, query)) {
		return;
	}
	if (ce_info.CanUpdate()) {
		ce_info.Delete();
		ce_info.MoveNext();
	}
	ce_info.Close();
	m_ceSet->Open();
	m_ceSet->Requery();
	m_ceSet->Close();
	UpdateDBView();
}
