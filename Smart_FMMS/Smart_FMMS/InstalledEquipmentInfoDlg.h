#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "InstalledEquipmentInfo.h"
// CInstalledEquipmentInfoDlg 대화 상자입니다.

class CInstalledEquipmentInfoDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CInstalledEquipmentInfoDlg)

public:
	CInstalledEquipmentInfoDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CInstalledEquipmentInfoDlg();
	CInstalledEquipmentInfo * m_ieSet;
	inline void InitDatabase(CInstalledEquipmentInfo* ieSet) {
		m_ieSet = ieSet;
	}
public:
// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_INSTALLEDEQUIPMENTINFO_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_DBView;
	void UpdateDBView();
	afx_msg void OnBnClickedLoad();
	virtual BOOL OnInitDialog();
protected:
	bool _inited;
	int m_index;
public:
	afx_msg void OnLvnItemchangedDbview(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedAdd();
	afx_msg void OnBnClickedEdit();
	afx_msg void OnBnClickedDelete();
	CButton m_Availability;
};
