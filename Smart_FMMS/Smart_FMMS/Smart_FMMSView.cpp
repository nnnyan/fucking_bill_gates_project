// Smart_FMMSView.cpp : CSmart_FMMSView 클래스의 구현
//
#include "stdafx.h"
// SHARED_HANDLERS는 미리 보기, 축소판 그림 및 검색 필터 처리기를 구현하는 ATL 프로젝트에서 정의할 수 있으며
// 해당 프로젝트와 문서 코드를 공유하도록 해 줍니다.
#ifndef SHARED_HANDLERS
#include "Smart_FMMS.h"
#endif

#include "ClosureInfo.h"
#include "Smart_FMMSDoc.h"
#include "Smart_FMMSView.h"
#include "InstalledEquipmentInfoDlg.h"
#include "EnterLogDlg.h"
#include "EventLogDlg.h"
#include "ClosureEquipmentInfoDlg.h"
#include "ClosureStateDlg.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CSmart_FMMSView

IMPLEMENT_DYNCREATE(CSmart_FMMSView, CRecordView)

BEGIN_MESSAGE_MAP(CSmart_FMMSView, CRecordView)
	// 표준 인쇄 명령입니다.
	ON_COMMAND(ID_FILE_PRINT, &CRecordView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CRecordView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CRecordView::OnFilePrintPreview)
	ON_BN_CLICKED(IDC_INSTALLED_BUTTON, &CSmart_FMMSView::OnBnClickedInstalledButton)
	ON_BN_CLICKED(IDC_EVENTLOG_BUTTON, &CSmart_FMMSView::OnBnClickedEventlogButton)
	ON_BN_CLICKED(IDC_ENTERLOG_BUTTON, &CSmart_FMMSView::OnBnClickedEnterlogButton)
	ON_BN_CLICKED(IDC_EQUIPMENT_BUTTON, &CSmart_FMMSView::OnBnClickedEquipmentButton)
	ON_BN_CLICKED(IDC_ADD, &CSmart_FMMSView::OnBnClickedAdd)
	ON_BN_CLICKED(IDC_EDIT, &CSmart_FMMSView::OnBnClickedEdit)
	ON_BN_CLICKED(IDC_DELETE, &CSmart_FMMSView::OnBnClickedDelete)
	ON_BN_CLICKED(IDC_LOAD, &CSmart_FMMSView::OnBnClickedLoad)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_DBVIEW, &CSmart_FMMSView::OnLvnItemchangedDbview)
	ON_BN_CLICKED(IDC_STATE_BUTTON, &CSmart_FMMSView::OnBnClickedStateButton)
END_MESSAGE_MAP()

// CSmart_FMMSView 생성/소멸

CSmart_FMMSView::CSmart_FMMSView()
	: CRecordView(IDD_SMART_FMMS_FORM)
{
	m_ieSet = NULL;
	m_entSet = NULL;
	m_evtSet = NULL;
	m_ceSet = NULL;
	m_ciSet = NULL;
	m_csSet = NULL;
	// TODO: 여기에 생성 코드를 추가합니다.
}

CSmart_FMMSView::~CSmart_FMMSView()
{
}

void CSmart_FMMSView::DoDataExchange(CDataExchange* pDX)
{
	CRecordView::DoDataExchange(pDX);
	// 컨트롤을 데이터베이스 필드에 '연결'하기 위해 여기에 DDX_Field* 함수를 삽입할 수 있습니다. 예:
	// DDX_FieldText(pDX, IDC_MYEDITBOX, m_pSet->m_szColumn1, m_pSet);
	// DDX_FieldCheck(pDX, IDC_MYCHECKBOX, m_pSet->m_bColumn2, m_pSet);
	// 자세한 내용은 MSDN 및 ODBC 샘플을 참조하십시오.
	DDX_Control(pDX, IDC_DBVIEW, m_DBView);
	DDX_Control(pDX, IDC_COMPLETIONDATE, m_CompletionDate);
	DDX_Control(pDX, IDC_AVAILABILITY, m_Availability);
}

BOOL CSmart_FMMSView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.
	return CRecordView::PreCreateWindow(cs);
}
struct com_set {
	CClosureInfo *ciSet;
	CClosureState *csSet;
	CEventLog *evSet;
	CEnterLog *enSet;
};
void start_server(com_set &);
#include <thread>
void CSmart_FMMSView::OnInitialUpdate()
{
	m_ciSet = &GetDocument()->m_ClosureInfoSet;
	CRecordView::OnInitialUpdate();
	UpdateDBView();
	m_DBView.SetExtendedStyle(LVS_EX_FULLROWSELECT);

	com_set cset;
	cset.ciSet = &GetDocument()->m_ClosureInfoSet;
	cset.csSet = &GetDocument()->m_ClosureStateSet;
	cset.evSet = &GetDocument()->m_EventLogSet;
	cset.enSet = &GetDocument()->m_EnterLogSet;
	std::thread *server_thread = new std::thread (start_server, cset);
}


// CSmart_FMMSView 인쇄

BOOL CSmart_FMMSView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 기본적인 준비
	return DoPreparePrinting(pInfo);
}

void CSmart_FMMSView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄하기 전에 추가 초기화 작업을 추가합니다.
}

void CSmart_FMMSView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄 후 정리 작업을 추가합니다.
}


// CSmart_FMMSView 진단

#ifdef _DEBUG
void CSmart_FMMSView::AssertValid() const
{
	CRecordView::AssertValid();
}

void CSmart_FMMSView::Dump(CDumpContext& dc) const
{
	CRecordView::Dump(dc);
}

CSmart_FMMSDoc* CSmart_FMMSView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CSmart_FMMSDoc)));
	return (CSmart_FMMSDoc*)m_pDocument;
}
#endif //_DEBUG


// CSmart_FMMSView 데이터베이스 지원
CRecordset* CSmart_FMMSView::OnGetRecordset()
{
	return m_ciSet;
}



// CSmart_FMMSView 메시지 처리기


void CSmart_FMMSView::OnBnClickedInstalledButton()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CInstalledEquipmentInfoDlg newdlg;
	newdlg.InitDatabase(&GetDocument()->m_InstalledEquipmentInfoSet);
	newdlg.DoModal();
}

void CSmart_FMMSView::OnBnClickedEnterlogButton()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CEnterLogDlg newdlg;
	newdlg.InitDatabase(&GetDocument()->m_EnterLogSet);
	newdlg.DoModal();
}

void CSmart_FMMSView::OnBnClickedEventlogButton()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CEventLogDlg newdlg;
	newdlg.InitDatabase(&GetDocument()->m_EventLogSet);
	newdlg.DoModal();
}

void CSmart_FMMSView::OnBnClickedEquipmentButton()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CClosureEquipmentInfoDlg newdlg;
	newdlg.InitDatabase(&GetDocument()->m_ClosureEquipmentInfoSet);
	newdlg.DoModal();
}

void CSmart_FMMSView::OnBnClickedStateButton()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CClosureStateDlg newdlg;
	newdlg.InitDatabase(&GetDocument()->m_ClosureStateSet);
	newdlg.DoModal();
}



void CSmart_FMMSView::UpdateDBView()
{
	static short int width = 100;
	static LPCTSTR name[] = 
	{ _T("일련번호"),_T("그룹이름1"),_T("그룹이름2"),_T("QR코드"),_T("함체ID"),_T("상태"),_T("함체위치명"),
		_T("설치구분"),_T("설치주소"),_T("위도"),_T("경도"),_T("IP주소"),_T("포트번호"),_T("준공일"),
		_T("설치업체"),_T("유지보수업체"),_T("업체연락처"),_T("사용여부") };
	static bool called = 0;
	if (!called) {
		for (int i = 0; i < 18; i++) {
			m_DBView.InsertColumn(i + 1, name[i], LVCFMT_LEFT, width);
		}
		called = 1;
	}
	m_DBView.DeleteAllItems();
	CClosureInfo closure_info(m_ciSet->m_pDatabase);
	closure_info.Open(CRecordset::dynaset, closure_info.GetDefaultSQL());
	for (int i = 0; !closure_info.IsEOF(); closure_info.MoveNext(), i++) {
		CString itemStr;
		itemStr.Format(_T("%d"), closure_info.m_Index);
		m_DBView.InsertItem(i, itemStr);
		m_DBView.SetItemText(i, 1, closure_info.m_GroupName1);
		m_DBView.SetItemText(i, 2, closure_info.m_GroupName2);
		m_DBView.SetItemText(i, 3, closure_info.m_QRCode);
		if (closure_info.m_ClosureID >= 0 && closure_info.m_ClosureID <= 65535) {
			itemStr.Format(_T("%d"), closure_info.m_ClosureID);
			m_DBView.SetItemText(i, 4, itemStr);
		}
		m_DBView.SetItemText(i, 5, closure_info.m_State);
		m_DBView.SetItemText(i, 6, closure_info.m_LocationName);
		m_DBView.SetItemText(i, 7, closure_info.m_InstallationClassification);
		m_DBView.SetItemText(i, 8, closure_info.m_InstallationAddress);
		m_DBView.SetItemText(i, 9, closure_info.m_latitude);
		m_DBView.SetItemText(i, 10, closure_info.m_longitude);
		m_DBView.SetItemText(i, 11, closure_info.m_IPAddress);
		if (closure_info.m_Port >= 0 && closure_info.m_Port <= 65535) {
			itemStr.Format(_T("%d"),closure_info.m_Port);
			m_DBView.SetItemText(i, 12, itemStr);
		}
		itemStr.Format(_T("%d-%d-%d"),
			closure_info.m_CompletionDate.GetYear(),
			closure_info.m_CompletionDate.GetMonth(),
			closure_info.m_CompletionDate.GetDay());
		m_DBView.SetItemText(i, 13, itemStr);
		m_DBView.SetItemText(i, 14, closure_info.m_InstallationCompany);
		m_DBView.SetItemText(i, 15, closure_info.m_MaintenanceCompany);
		m_DBView.SetItemText(i, 16, closure_info.m_CompanyTelephone);
		m_DBView.SetItemText(i, 17, closure_info.m_Availability ? _T("Yes") : _T("No"));
	}
	closure_info.Close();
}


void CSmart_FMMSView::OnBnClickedAdd()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CClosureInfo closure_info(m_ciSet->m_pDatabase);
	if (!closure_info.Open()) {
		return;
	}
	closure_info.AddNew();
	closure_info.m_Index = _ttoi(m_DBView.GetItemText(m_DBView.GetItemCount() - 1, 0)) + 1;
	GetDlgItemText(IDC_GROUPNAME1, closure_info.m_GroupName1);
	GetDlgItemText(IDC_GROUPNAME2, closure_info.m_GroupName2);
	GetDlgItemText(IDC_QRCODE, closure_info.m_QRCode);
	GetDlgItemText(IDC_LOCATIONNAME, closure_info.m_LocationName);
	CString closureID;
	GetDlgItemText(IDC_CLOSUREID, closureID);
	closure_info.m_ClosureID = _ttoi(closureID);
	GetDlgItemText(IDC_STATE, closure_info.m_State);
	GetDlgItemText(IDC_INSTALLATIONCLASSIFICATION, closure_info.m_InstallationClassification);
	GetDlgItemText(IDC_INSTALLATIONADDRESS, closure_info.m_InstallationAddress);
	GetDlgItemText(IDC_LATITUDE, closure_info.m_latitude);
	GetDlgItemText(IDC_LONGITUDE, closure_info.m_longitude);
	GetDlgItemText(IDC_IPADDRESS, closure_info.m_IPAddress);
	CString port;
	GetDlgItemText(IDC_PORT, port);
	closure_info.m_Port = _ttoi(port);
	COleDateTime date;
	m_CompletionDate.GetTime(date);
	closure_info.m_CompletionDate = date;
	GetDlgItemText(IDC_INSTALLATIONCOMPANY, closure_info.m_InstallationCompany);
	GetDlgItemText(IDC_MAINTENANCECOMPANY, closure_info.m_MaintenanceCompany);
	CString phone[3];
	GetDlgItemText(IDC_COMPANYTELEPHONE1, phone[0]);
	GetDlgItemText(IDC_COMPANYTELEPHONE2, phone[1]);
	GetDlgItemText(IDC_COMPANYTELEPHONE3, phone[2]);
	closure_info.m_CompanyTelephone = phone[0] + "-" + phone[1] + "-" + phone[2];
	GetDlgItemText(IDC_IPADDRESS, closure_info.m_IPAddress);
	closure_info.m_Availability = m_Availability.GetCheck();
	if (!closure_info.Update()) {
		AfxMessageBox(_T("Record not added; no field values were set."));
	}
	closure_info.Requery();
	closure_info.Close();
	UpdateDBView();
}


void CSmart_FMMSView::OnBnClickedEdit()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CClosureInfo closure_info(m_ciSet->m_pDatabase);
	CString query;
	query.Format(_T("%s WHERE Index = %d"), closure_info.GetDefaultSQL(), m_index);
	if (!closure_info.Open(CRecordset::dynaset, query)) {
		return;
	}
	closure_info.Edit();
	GetDlgItemText(IDC_GROUPNAME1, closure_info.m_GroupName1);
	GetDlgItemText(IDC_GROUPNAME2, closure_info.m_GroupName2);
	GetDlgItemText(IDC_QRCODE, closure_info.m_QRCode);
	GetDlgItemText(IDC_LOCATIONNAME, closure_info.m_LocationName);
	CString closureID;
	GetDlgItemText(IDC_CLOSUREID, closureID);
	closure_info.m_ClosureID = _ttoi(closureID);
	GetDlgItemText(IDC_STATE, closure_info.m_State);
	GetDlgItemText(IDC_INSTALLATIONCLASSIFICATION, closure_info.m_InstallationClassification);
	GetDlgItemText(IDC_INSTALLATIONADDRESS, closure_info.m_InstallationAddress);
	GetDlgItemText(IDC_LATITUDE, closure_info.m_latitude);
	GetDlgItemText(IDC_LONGITUDE, closure_info.m_longitude);
	GetDlgItemText(IDC_IPADDRESS, closure_info.m_IPAddress);
	CString port;
	GetDlgItemText(IDC_PORT, port);
	closure_info.m_Port = _ttoi(port);
	COleDateTime date;
	m_CompletionDate.GetTime(date);
	closure_info.m_CompletionDate = date;
	GetDlgItemText(IDC_INSTALLATIONCOMPANY, closure_info.m_InstallationCompany);
	GetDlgItemText(IDC_MAINTENANCECOMPANY, closure_info.m_MaintenanceCompany);
	CString phone[3];
	GetDlgItemText(IDC_COMPANYTELEPHONE1, phone[0]);
	GetDlgItemText(IDC_COMPANYTELEPHONE2, phone[1]);
	GetDlgItemText(IDC_COMPANYTELEPHONE3, phone[2]);
	closure_info.m_CompanyTelephone = phone[0] + "-" + phone[1] + "-" + phone[2];
	GetDlgItemText(IDC_IPADDRESS, closure_info.m_IPAddress);
	closure_info.m_Availability = m_Availability.GetCheck();
	if (!closure_info.Update())
	{
		AfxMessageBox(_T("Record not updated; no field values were set."));
	}
	closure_info.Requery();
	closure_info.Close();
	UpdateDBView();
}


void CSmart_FMMSView::OnBnClickedDelete()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CClosureInfo closure_info(m_ciSet->m_pDatabase);
	CString query;
	query.Format(_T("%s WHERE Index = %d"), closure_info.GetDefaultSQL(), m_index);
	if (!closure_info.Open(CRecordset::dynaset, query)) {
	}
	if (closure_info.CanUpdate()) {
		closure_info.Delete();
		closure_info.MoveNext();
	}
	closure_info.Requery();
	closure_info.Close();
	UpdateDBView();
}


void CSmart_FMMSView::OnBnClickedLoad()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateDBView();
}


void CSmart_FMMSView::OnLvnItemchangedDbview(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int index = pNMLV->iItem;
	if ((m_DBView.GetItemState(index, LVIS_SELECTED) & LVIS_SELECTED) && index >= 0 && index < m_DBView.GetItemCount())
	{
		m_index = _ttoi(m_DBView.GetItemText(index, 0));
		SetDlgItemText(IDC_GROUPNAME1, m_DBView.GetItemText(index, 1));
		SetDlgItemText(IDC_GROUPNAME2, m_DBView.GetItemText(index, 2));
		SetDlgItemText(IDC_QRCODE, m_DBView.GetItemText(index, 3));
		SetDlgItemText(IDC_LOCATIONNAME, m_DBView.GetItemText(index, 4));
		SetDlgItemText(IDC_CLOSUREID, m_DBView.GetItemText(index, 6));
		SetDlgItemText(IDC_STATE, m_DBView.GetItemText(index, 5));
		SetDlgItemText(IDC_INSTALLATIONCLASSIFICATION, m_DBView.GetItemText(index, 7));
		SetDlgItemText(IDC_INSTALLATIONADDRESS, m_DBView.GetItemText(index, 8));
		SetDlgItemText(IDC_LATITUDE, m_DBView.GetItemText(index, 9));
		SetDlgItemText(IDC_LONGITUDE, m_DBView.GetItemText(index, 10));
		SetDlgItemText(IDC_IPADDRESS, m_DBView.GetItemText(index, 11));
		SetDlgItemText(IDC_PORT, m_DBView.GetItemText(index, 12));
		int pos = 0, year, month, day;
		CString date_str = m_DBView.GetItemText(index, 13), token;
		date_str.Remove(_T('년'));
		date_str.Remove(_T('월'));
		date_str.Remove(_T('일'));
		if ((token = date_str.Tokenize(_T(" "), pos)) != "") {
			year = _ttoi(token);
		}
		if ((token = date_str.Tokenize(_T(" "), pos)) != "") {
			month = _ttoi(token);
		}
		if ((token = date_str.Tokenize(_T(" "), pos)) != "") {
			day = _ttoi(token);
		}
		COleDateTime date;
		date.SetDate(year, month, day);
		m_CompletionDate.SetTime(date);
		SetDlgItemText(IDC_INSTALLATIONCOMPANY, m_DBView.GetItemText(index, 14));
		SetDlgItemText(IDC_MAINTENANCECOMPANY, m_DBView.GetItemText(index, 15));
		CString phone_number = m_DBView.GetItemText(index, 16);
		pos = 0;
		if (phone_number.GetLength() >= 10 &&
			phone_number.Find(_T("-")) >= 0) {
			if ((token = phone_number.Tokenize(_T("-"), pos)) != "") {
				SetDlgItemText(IDC_COMPANYTELEPHONE1, token);
			}
			if ((token = phone_number.Tokenize(_T("-"), pos)) != "") {
				SetDlgItemText(IDC_COMPANYTELEPHONE2, token);
			}
			if ((token = phone_number.Tokenize(_T("-"), pos)) != "") {
				SetDlgItemText(IDC_COMPANYTELEPHONE3, token);
			}
		}
		bool cond = m_DBView.GetItemText(index, 17) == "Yes" ? true : false;
		m_Availability.SetCheck(cond);
	}
	*pResult = 0;
}








#define EVENT_TYPE_DOOR 0

#pragma pack(push, 1)
struct packet_client {
	int cctv_no;
	uint64_t timestamp;
	float temperature;
	float humidity;
	int event_type;
	int remote;
	unsigned char entered;
	/*
	* entered:
	* 0: Open DOOR1
	* 1: Open DOOR2
	* 2: Close DOOR1
	* 3: Close DOOR2
	* 4~7: undefined
	*/

	unsigned char flags;
	/*
	* flags:
	* 0: DOOR1
	* 1: DOOR2
	* 2: TCP/IP
	* 3: RF
	* 4: SIREN
	* 5: SENSOR
	* 6~7: undefined
	*/

	// unsigned char padding[2];
};
#pragma pack(pop)

#pragma pack(push, 1)
struct packet_server {
	unsigned char ok;
	// unsigned char padding[3];
};
#pragma pack(pop)


#include "ClosureState.h"
#include <vector>
#include <clocale>
void client_session(SOCKET client_socket, com_set &comset) {

	struct packet_client received;
	struct packet_server to_send;
	static std::vector<packet_client> packet_vector;
	while (1) {
		memset(&received, 0, sizeof(struct packet_client));
		if (recv(client_socket, (char *)&received, sizeof(struct packet_client), 0) > 0) {
			packet_vector.push_back(received);
			for (std::vector<packet_client>::size_type index = 0; index < packet_vector.size(); ++index) {
				auto i = packet_vector[index];
				CClosureInfo ci(comset.ciSet->m_pDatabase);
				CString query, ci_LocationName, ci_GroupName1, ci_InstallationCompany;
				query.Format(_T("%s WHERE Index = %d"), ci.GetDefaultSQL(), i.cctv_no);
				if (ci.Open(CRecordset::dynaset, query)) {
					ci_LocationName = ci.m_LocationName;
					ci_GroupName1 = ci.m_GroupName1;
					ci_InstallationCompany = ci.m_InstallationCompany;
					ci.Close();
				}
				else {
					break;
				}
				CClosureState cs(comset.csSet->m_pDatabase);
				query.Format(_T("%s WHERE LocationName = '%s'"), cs.GetDefaultSQL(), ci_LocationName);
				if (cs.Open(CRecordset::dynaset, query)) {
					cs.Edit();
					cs.m_LocationName = ci_LocationName;
					cs.m_Door1 = !((i.flags >> 7) & 0x1);
					cs.m_Door2 = !((i.flags >> 6) & 0x1);
					cs.m_TCPIP = !((i.flags >> 5) & 0x1);
					cs.m_RF = !((i.flags >> 4) & 0x1);
					cs.m_Siren = !((i.flags >> 3) & 0x1);
					cs.m_Sensor = !((i.flags >> 2) & 0x1);
					CString temp,hum;
					temp.Format(_T("%.2f"), i.temperature);
					cs.m_Temerature = temp;
					hum.Format(_T("%.2f"), i.humidity);
					cs.m_Humidity = hum;
					if (!cs.Update())
					{
						AfxMessageBox(_T("Record not updated; no field values were set."));
						break;
					}


					if (i.entered > 0) {
						CEnterLog en(comset.enSet->m_pDatabase);
						if (!en.Open()) {
							break;
						}
						en.AddNew();
						en.m_LocationName = ci_LocationName;
						en.m_GroupName1 = ci_GroupName1;
						en.m_GroupName2 = ci_InstallationCompany;
						en.m_PasserName = _T("관리자");
						if ((i.entered >> 7) & 0x1) { // Door1 Open
							en.m_OpenDoor1 = CTime(i.timestamp);
						}
						else if ((i.entered >> 6) & 0x1) { // Door2 Open
							en.m_OpenDoor2 = CTime(i.timestamp);
						}
						else if ((i.entered >> 5) & 0x1) { // Door1 Close
							en.m_CloseDoor1 = CTime(i.timestamp);
						}
						else if ((i.entered >> 4) & 0x1) { // Door2 Close
							en.m_CloseDoor2 = CTime(i.timestamp);
						}
						if (!en.Update()) {
							AfxMessageBox(_T("Record not added; no field values were set."));
							break;
						}
						en.Close();
						comset.enSet->Open();
						comset.enSet->Requery();
						comset.enSet->Close();


						packet_vector.pop_back();
					}
					else {
						packet_vector.pop_back();
					}
				}
				else {
					break;
				}
			}
			memset(&to_send, 0, sizeof(struct packet_server));
			to_send.ok = 1;
			send(client_socket, (char *)&to_send, sizeof(struct packet_server), 0);
		}
	}
}


void start_server(com_set &comset) {
	int port = 30001;
	int socket_len = sizeof(struct sockaddr_in);
	WSADATA wsaData;
	SOCKET server_socket, client_socket;
	struct sockaddr_in server_addr, client_addr;

	if (WSAStartup(MAKEWORD(2, 2), &wsaData)) {
		printf("WSAStartup error\n");
		return;
	}

	server_socket = socket(AF_INET, SOCK_STREAM, 0);
	if (server_socket == INVALID_SOCKET) {
		printf("socket error\n");
		return;
	}

	memset(&server_addr, 0, sizeof(struct sockaddr_in));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.S_un.S_addr = htonl(INADDR_ANY);
	server_addr.sin_port = htons(port);

	if (bind(server_socket, (struct sockaddr *)&server_addr, sizeof(struct sockaddr_in))
		== SOCKET_ERROR) {
		printf("bind error\n");
		return;
	}

	if (listen(server_socket, 5) == SOCKET_ERROR) {
		printf("listen error\n");
		return;
	}

	printf("listening\n");

	while (1) {
		memset(&client_addr, 0, sizeof(struct sockaddr_in));
		socket_len = sizeof(struct sockaddr_in);
		client_socket = accept(server_socket, (struct sockaddr *)&client_addr, &socket_len);
		if (client_socket == INVALID_SOCKET) {
			printf("accept error\n");
			closesocket(server_socket);
			WSACleanup();
			return;
		}
		std::thread *client_thread = new std::thread(client_session, client_socket, comset);
	}
}
