// EnterLog.h : CEnterLog 클래스의 구현입니다.



// CEnterLog 구현입니다.

// 코드 생성 위치 2018년 11월 25일 일요일, 오전 1:29

#include "stdafx.h"
#include "EnterLog.h"
IMPLEMENT_DYNAMIC(CEnterLog, CRecordset)

CEnterLog::CEnterLog(CDatabase* pdb)
	: CRecordset(pdb)
{
	m_GroupName1 = L"";
	m_GroupName2 = L"";
	m_LocationName = L"";
	m_PasserName = L"";
	m_OpenDoor1;
	m_OpenDoor2;
	m_CloseDoor1;
	m_CloseDoor2;
	m_nFields = 8;
	m_nDefaultType = dynaset;
}
// 아래 연결 문자열에 일반 텍스트 암호 및/또는 
// 다른 중요한 정보가 포함되어 있을 수 있습니다.
// 보안 관련 문제가 있는지 연결 문자열을 검토한 후에 #error을(를) 제거하십시오.
// 다른 형식으로 암호를 저장하거나 다른 사용자 인증을 사용하십시오.
CString CEnterLog::GetDefaultConnect()
{
	return _T("DSN=Smart_FMMS;DBQ=DatabaseDesignEx.mdb;DriverId=25;FIL=MS Access;MaxBufferSize=2048;PageTimeout=5;UID=admin;");
}

CString CEnterLog::GetDefaultSQL()
{
	return _T("[출입보안 로그]");
}

void CEnterLog::DoFieldExchange(CFieldExchange* pFX)
{
	pFX->SetFieldType(CFieldExchange::outputColumn);
// RFX_Text() 및 RFX_Int() 같은 매크로는 데이터베이스의 필드
// 형식이 아니라 멤버 변수의 형식에 따라 달라집니다.
// ODBC에서는 자동으로 열 값을 요청된 형식으로 변환하려고 합니다
	RFX_Text(pFX, _T("[GroupName1]"), m_GroupName1);
	RFX_Text(pFX, _T("[GroupName2]"), m_GroupName2);
	RFX_Text(pFX, _T("[LocationName]"), m_LocationName);
	RFX_Text(pFX, _T("[PasserName]"), m_PasserName);
	RFX_Date(pFX, _T("[Open Door1]"), m_OpenDoor1);
	RFX_Date(pFX, _T("[Open Door2]"), m_OpenDoor2);
	RFX_Date(pFX, _T("[Close Door1]"), m_CloseDoor1);
	RFX_Date(pFX, _T("[Close Door2]"), m_CloseDoor2);

}
/////////////////////////////////////////////////////////////////////////////
// CEnterLog 진단

#ifdef _DEBUG
void CEnterLog::AssertValid() const
{
	CRecordset::AssertValid();
}

void CEnterLog::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG


